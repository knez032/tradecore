create-social-network::
	sh -c "sudo docker-compose up --no-start"
	sh -c "sudo docker start postgres_social_network"
	sh -c "sudo docker start social_network"
	sh -c "sudo docker exec -it social_network python manage.py migrate"

run-bot::
	sh -c "sudo docker exec -it social_network python manage.py bot --data /socialnetwork/management/data/data.json