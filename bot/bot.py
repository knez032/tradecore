import base64
import json
import logging
import random

import requests
from faker import Faker
from rest_framework import status

from socialnetwork.utils import password_generator, generate_personal_info
from .exceptions import FailToSignUpUser, FailToLoginUser, FailToCreatePost, FailToLikePost, FailToGetPost

logger = logging.getLogger(__name__)

HEADERS = {
    "content-type": "application/json",
    "Authorization": ""
}
SIGN_UP_URL = 'http://localhost:8000/signup/'
LOGIN_URL = 'http://localhost:8000/login/'
POST_URL = 'http://localhost:8000/posts/'
LIKE_URL = 'http://localhost:8000/posts/{id}/like/'
GET_TOKEN_URL = 'http://localhost:8000/api-token-auth/'


class Bot(object):
    def __init__(self, data):
        self.data = data

        self.number_of_users = self.data.get('number_of_users', 0)
        self.max_posts_per_user = self.data.get('max_posts_per_user', 0)
        self.max_likes_pre_user = self.data.get('max_likes_per_user', 0)
        self.users = []

    def signup_users(self):
        for i in range(self.number_of_users):
            personal_info = generate_personal_info()
            payload = {
                "type": base64.b64encode(b'bot').decode('utf-8'),
                "password": password_generator(),
                "username": personal_info['username'],
                "first_name": personal_info['first_name'],
                "last_name": personal_info['last_name'],
                "email": personal_info['username'] + "@bot.com"
            }
            response = requests.post(url=SIGN_UP_URL, data=json.dumps(payload), headers=HEADERS)
            if not response.status_code == status.HTTP_201_CREATED:
                raise FailToSignUpUser()
            # update payload with with user id, number_of_post and number_of_likes
            payload.update(
                {
                    'id': json.loads(response.content)['id'],
                    'number_of_posts': random.randint(1, self.max_posts_per_user),
                    'number_of_likes': random.randint(1, self.max_likes_pre_user)
                }
            )
            # add user info to users list
            self.users.append(payload)
            logger.info("Signup user {}".format(payload['username']))

    @staticmethod
    def login_user(data):
        payload = {
            "username": data["username"],
            "password": data["password"]
        }
        response = requests.post(url=GET_TOKEN_URL, data=json.dumps(payload), headers=HEADERS)
        if response.status_code == status.HTTP_200_OK:
            return json.loads(response.content)['token']
        else:
            raise FailToLoginUser()

    def create_posts(self):
        fake = Faker()
        for user in self.users:
            header = HEADERS
            header['Authorization'] = 'JWT ' + self.login_user(user)
            for i in range(user['number_of_posts']):
                payload = {
                    "text": fake.sentence()
                }
                response = requests.post(url=POST_URL, data=json.dumps(payload), headers=header)
                if response.status_code != status.HTTP_200_OK:
                    raise FailToCreatePost()

    def perform_likes(self):
        for user in sorted(self.users, key=lambda k: k['number_of_posts'], reverse=True):
            header = HEADERS
            header['Authorization'] = 'JWT ' + self.login_user(user)
            for _ in range(user['number_of_likes']):
                response = requests.get(url=POST_URL, headers=header)
                if response.status_code != status.HTTP_200_OK:
                    raise FailToGetPost()
                posts = json.loads(response.content)
                possible_user_ids = set([i['author']['id'] for i in filter(lambda x: not x['likes'], posts) if
                                         i['author']['id'] != user['id']])
                possible_posts_ids = [i['id'] for i in posts if
                                      i['author']['id'] in possible_user_ids and user['id'] not in i['likes']]
                if not filter(lambda x: not x['likes'], posts):
                    return "Bot has finished like."
                elif possible_posts_ids:
                    post_id = random.choice(possible_posts_ids)
                    response = requests.get(url=LIKE_URL.format(id=post_id), headers=header)
                    if response.status_code == status.HTTP_200_OK:
                        logger.info("User {} like post with id {}".format(user['username'], post_id))
                    else:
                        raise FailToLikePost()
                else:
                    continue
        logger.info("Done booting")
