class FailToSignUpUser(Exception):
    pass


class FailToLoginUser(Exception):
    pass


class FailToCreatePost(Exception):
    pass


class FailToGetPost(Exception):
    pass


class FailToLikePost(Exception):
    pass
