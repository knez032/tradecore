import string
from random import choice, randint

import clearbit
import names
from django.conf import settings
from requests import request


def check_existence(email):
    """
    Check existence of provided email using email hunter service.
    """
    base_url = "https://api.hunter.io/v2/email-verifier"
    payload = {
        "api_key": settings.EMAIL_HUNTER_API_KEY,
        "email": email
    }
    response = request("GET", base_url, data=payload)
    response.raise_for_status()
    data = response.json()
    if data["data"]['result'] == "undeliverable":
        return False
    else:
        return True


def enrich_data(email):
    """
    Gets addition data for user with provided email. 
    :param email: 
    :return: 
    """
    additional_data = {}
    response = clearbit.Enrichment.find(email=email)
    if response and response.get('person'):
        # can be extended for more data
        additional_data['first_name'] = response['person']['name']['givenName']
        additional_data['last_name'] = response['person']['name']['familyName']
    return additional_data


def password_generator(min_char=8, max_char=14):
    characters = string.ascii_letters + string.punctuation + string.digits
    password = "".join(choice(characters) for _ in range(randint(min_char, max_char)))
    return password


def generate_personal_info():
    first_name = names.get_first_name()
    last_name = names.get_last_name()
    username = first_name.lower() + "." + last_name.lower() + "." + choice(string.digits)
    return {
        "first_name": first_name,
        "last_name": last_name,
        "username": username
    }
