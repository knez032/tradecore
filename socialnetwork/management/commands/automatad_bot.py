import json
import os

from django.core.management.base import BaseCommand

from bot.bot import Bot


class Command(BaseCommand):
    help = "Bot command that sets users, posts and likes"
    BASEDIR = os.getcwd()

    def add_arguments(self, parser):
        parser.add_argument('--data', nargs='?', type=str)

    def handle(self, *args, **options):
        data = json.load(open(options.get("data"), 'r'))
        bot = Bot(data=data)

        # signup users
        bot.signup_users()

        # create posts
        bot.create_posts()

        # run bot liking process
        bot.perform_likes()
