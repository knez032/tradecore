SocialNetwork
Django REST API

App dockerization:
- Create app and db containers: make create-social-network
- Run migrations: make migrate-social

Bot usage:
- python manage.py bot --data json_file_path
there is example data in /socialnetwork/management/data/data.json
- If you are using app from docker then run:
make run-bot

Docs:
- /docs/