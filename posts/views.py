# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework import viewsets
from rest_framework.decorators import detail_route
from rest_framework.response import Response

from posts.models import Post
from posts.serializers import PostSerializer
from socialnetwork.permissions import IsOwnerOrReadOnly


class PostView(viewsets.ModelViewSet):
    """
    retrieve:
        Return a post instance.

    list:
        Return all post, ordered by most recently posted.

    create:
        Create a new post.

    delete:
        Remove an existing post.

    partial_update:
        Update one or more fields on an existing post.

    update:
        Update a post.
    """
    permission_classes = (IsOwnerOrReadOnly,)  # User can edit just his own post
    serializer_class = PostSerializer
    queryset = Post.objects.select_related('author').prefetch_related("likes").all()
    ordering = ('added',)

    @detail_route(methods=['GET'])
    def like(self, request, *args, **kwargs):
        """
        Like action, it works as a toggle.
        """
        user = request.user
        post = self.get_object()
        if user in post.likes.all():
            post.likes.remove(user)
        else:
            post.likes.add(user)

        return Response(PostSerializer(post).data['likes'])

    def create(self, request, *args, **kwargs):
        # adding author from the request
        data = request.data.copy()
        data['author'] = self.request.user.id

        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return Response(serializer.data)
