from rest_framework import routers

from posts.views import PostView

router = routers.SimpleRouter()
router.register(r'posts', PostView)

urlpatterns = router.urls
