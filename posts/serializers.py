from django.contrib.auth import get_user_model
from drf_extra_fields.relations import PresentablePrimaryKeyRelatedField
from rest_framework import serializers

from posts.models import Post
from users.serializers import UserSerializerCropped

User = get_user_model()


class PostSerializer(serializers.ModelSerializer):
    author = PresentablePrimaryKeyRelatedField(
        queryset=User.objects,
        presentation_serializer=UserSerializerCropped
    )

    class Meta:
        model = Post
        fields = '__all__'
