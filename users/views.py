# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import base64

from django.contrib.auth import get_user_model
from rest_framework import viewsets
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import IsAuthenticated, AllowAny

from socialnetwork.permissions import IsOwnerOrReadOnly
from users.serializers import UserSerializer, SignUpSerializer

User = get_user_model()


class UserView(viewsets.ReadOnlyModelViewSet):
    """
    User view.
    
    retrieve:
        Return a user instance.

    list:
        Return all users, ordered by most recently joined.
    """
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly]


class SignUpView(CreateAPIView):
    """
    User signup view.
    """
    queryset = User.objects.all()
    permission_classes = [AllowAny]
    serializer_class = SignUpSerializer

    def get_serializer_context(self):
        # Extend context of a view so we can somehow determine that bot is creating a request
        # so we can skip checking of existence of email and getting the additional data
        context = super(SignUpView, self).get_serializer_context()
        context.update(
            {'type': base64.b64decode(self.request.data.pop('type', '')).decode('utf-8')}
        )
        return context
