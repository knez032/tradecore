from django.conf.urls import url, include
from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token

from users.views import UserView, SignUpView

router = routers.SimpleRouter()
router.register(r'users', UserView, base_name='users')

urlpatterns = [
    url(r'^signup/', SignUpView.as_view(), name='signup'),
]

urlpatterns += router.urls
