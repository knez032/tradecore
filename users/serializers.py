from django.conf import settings
from django.contrib.auth import get_user_model
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from socialnetwork.utils import check_existence, enrich_data

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'


class UserSerializerCropped(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name')


class SignUpSerializer(serializers.ModelSerializer):
    username = serializers.CharField(
        validators=[UniqueValidator(queryset=User.objects.all())]
    )
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )
    password = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = ('id', 'email', 'username', 'password', 'first_name', 'last_name')

    def validate_email(self, value):
        if self.context.get('type') == settings.BOT_TYPE:
            return value
        if not check_existence(email=value):
            raise serializers.ValidationError("This email can't be validated")
        return value

    def create(self, validated_data):
        # Additional data should be set in some task because this can be long running task
        # this task should be triggered on post_save signal on User model
        if self.context.get('type') == settings.BOT_TYPE:
            addition_data = {
                'first_name': validated_data['first_name'],
                'last_name': validated_data['last_name']
            }
        else:
            addition_data = enrich_data(email=validated_data['email'])
        user = User(
            email=validated_data["email"],
            username=validated_data["username"],
            **addition_data
        )
        user.set_password(validated_data["password"])
        user.save()
        return user
