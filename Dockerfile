FROM python:2

LABEL authors="Nebojsa Knezevic nebojsa.knezevic032@gmail.com"

COPY . /social_network
RUN pip install -r /social_network/requirements.txt

WORKDIR /social_network
EXPOSE 8000
CMD gunicorn --workers=4 --max-requests 100 -t 200 -b 0.0.0.0:8000 socialnetwork.wsgi